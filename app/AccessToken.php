<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AccessToken extends Model
{
    function user() {
        return $this->belongsTo('App\User');
    }

    protected $fillable = [
        'user_id',
        'access_token',
        'refresh_token'
    ];

    protected $hidden = [
        'id',
        'user_id',
        'refresh_token',
        'created_at',
        'updated_at',
    ];
}
