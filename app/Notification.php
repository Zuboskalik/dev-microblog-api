<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
  public function users() {
      return $this->belongsToMany('App\User', 'notification_user', 'notifications_id', 'users_id');
  }

  protected $fillable = [
      'created_at',
      'addressed_to',
      'text'
  ];
}
