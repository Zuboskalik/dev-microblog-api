<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Comment extends Model
{

    public function record() {
        return $this->belongsTo('App\Record');
    }

    protected $fillable = [
        'user_id',
        'record_id',
        'text',
    ];

    protected $hidden = [
        'updated_at',
    ];
}
