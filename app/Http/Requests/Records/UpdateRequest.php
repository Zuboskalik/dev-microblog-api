<?php

namespace App\Http\Requests\Records;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

use App\Record;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $id = Request::Input('id');
        $user = Request::Input('user');

        $record = Record::find($id);
        //return $comment->user_id == $this->user->id;

        return $record->user_id == $user['id'];
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function rules()
    {
        return [
            'id'        => 'required|integer|exists:records',
            'text'      => 'required|string|max:140'
        ];
    }
}
