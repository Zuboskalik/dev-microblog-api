<?php

namespace App\Http\Requests\Comments;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

use App\Record;
use App\Comment;

class DeleteRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $id = Request::Input('id');
        $user = Request::Input('user');

        $comment = Comment::find($id);
        $record = Record::find($comment->record_id);

        return $comment->user_id == $user['id'] or $record->user_id == $user['id'];
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => 'required|integer|exists:comments'
        ];
    }
}
