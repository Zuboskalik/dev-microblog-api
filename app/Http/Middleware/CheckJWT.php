<?php

namespace App\Http\Middleware;

use Closure;

use App\AccessToken;

class CheckJWT
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
      $guestAddresses = [
        'api/v1/records/index',
        'api/v1/comments/index',
      ];
      // Входит ли адрес в список разрешенных для всех
      foreach($guestAddresses as $guestAddress) {
        if($request->is($guestAddress)) {
          return $next($request);
        }
      }

        $token = $request->header('Authorization');
        if(!$token or $token == 'undefined') {
            abort(403, trans('locale.errors.bad_token'));
        }
        $token = explode(' ', $token)[1];

        $access_token = AccessToken::where('access_token', $token)->first();

        // Если токена нет, сообщаем об этом
        if(!$access_token) {
          abort(403, trans('locale.errors.bad_token'));
        }

        // Сохраним эти параметры для передачи дальше
        $request['user'] = $access_token->user;
        $request['access_token'] = $access_token;

        return $next($request);
    }
}
