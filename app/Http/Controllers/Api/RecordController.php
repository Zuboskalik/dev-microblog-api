<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Records\IndexRequest;
use App\Http\Requests\Records\StoreRequest;
use App\Http\Requests\Records\UpdateRequest;
use App\Http\Requests\Records\DeleteRequest;
use App\User;
use App\Record;
use App\Comment;
use DB;
use Illuminate\Http\Request;
use Storage;

class RecordController extends Controller
{
    public function index(IndexRequest $request)
    {
        $records = new Record();

        $records = $records
        ->orderByDesc('created_at')
            ->paginate(25);

        foreach ($records as $record) {
            /*$record->setAppends([
                'user',
                'comments',
            ]);*/
            $record->user = User::Find($record->user_id);
            $record->comments = Comment::Where('record_id', $record->id)->get();
            foreach ($record->comments as $comment) {
              $comment->user = User::Find($comment->user_id);
            }
        }

        return $records;
    }

    public function store(StoreRequest $request)
    {

        $data = $request->post();

        $user = User::Find($request->user_id);
        // Создать заказ
        /*$record = $user
            ->records()
            ->create($data);*/

        $record = Record::create($data);

        /*$record->setAppends([
            'user',
            'comments'
        ]);*/
        $record->user = User::Find($record->user_id);
        $record->comments = Comment::Where('record_id', $record->id)->get();
        foreach ($record->comments as $comment) {
          $comment->user = User::Find($comment->user_id);
        }

        return $record;
    }

    public function update(UpdateRequest $request)
    {
        $data = $request->post();

        $record = Record::find($request->id);

        $record->update($data);

        $record->user = User::Find($record->user_id);
        /*$record->comments = Comment::Where('record_id', $record->id)->get();
        foreach ($record->comments as $comment) {
          $comment->user = User::Find($comment->user_id);
        }*/

        return $record;
    }

    public function delete(DeleteRequest $request)
    {
        $record = Record::find($request->id);

        $comments = Comment::Where('record_id', $record->id);
        $comments->delete();

        $record->delete();

        return null;
    }

}
