<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Comments\IndexRequest;
use App\Http\Requests\Comments\StoreRequest;
use App\Http\Requests\Comments\UpdateRequest;
use App\Http\Requests\Comments\DeleteRequest;
use App\User;
use App\Record;
use App\Comment;
use App\Notification;
use DB;
use Illuminate\Http\Request;
use Storage;

class CommentController extends Controller
{
    public function index(IndexRequest $request)
    {
        $comments = new Comment();

        $comments = $comments
        ->orderByDesc('created_at')
            ->paginate(25);

        foreach ($comments as $comment) {
            /*$comment->setAppends([
                'user'
            ]);*/
            $comment->user = User::Find($request->user_id);
        }

        return $comments;
    }

    public function store(StoreRequest $request)
    {

        $data = $request->post();

        // Создать
        /*$comment = $record
            ->comments()
            ->create($data);*/

        $comment = Comment::create($data);
        /*$comment->setAppends([
            'user'
        ]);*/
        $comment->user = User::Find($request->user_id);
        $addressed_to = Record::Find($request->record_id)->user_id;

        Notification::create(['addressed_to' => $addressed_to, 'text' => 'У вас новый комментарий к записи #'.$data['record_id'].': "'.$data['text'].'"']);

        return $comment;
    }

    public function update(UpdateRequest $request)
    {
        $data = $request->post();

        $comment = Comment::find($request->id);

        $comment->update($data);

        $comment->user = User::Find($request->user_id);

        return $comment;
    }

    public function delete(DeleteRequest $request)
    {
        $comment = Comment::find($request->id);

        $comment->delete();

        return null;
    }

}
