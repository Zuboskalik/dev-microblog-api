<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Notifications\IndexRequest;
use App\User;
use App\Record;
use App\Comment;
use App\Notification;
use DB;
use Illuminate\Http\Request;
use Storage;

class NotificationController extends Controller
{
    public function index(IndexRequest $request)
    {
        $notifications = new Notification();
        $user = $request->user_id;

        $notifications = $notifications->where('addressed_to', $user)->get();

        Notification::destroy($notifications->pluck('id')->all());

        return $notifications;
    }

}
