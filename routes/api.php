<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('register', 'Auth\AuthController@register')->name('register');
Route::post('login', 'Auth\AuthController@login')->name('login');
Route::get('/user', 'Auth\AuthController@user');
Route::post('/logout', 'Auth\AuthController@logout');

Route::group(['prefix' => 'v1', 'middleware' => 'CheckJWT'], function () {

      // Уведомления
      Route::group(['prefix' => 'notifications'], function () {
          // Получить
          Route::post('/index', 'Api\NotificationController@index');
      });

      // Записи
      Route::group(['prefix' => 'records'], function () {
          // Получить
          Route::get('/index', 'Api\RecordController@index');
          // Создать
          Route::post('/store', 'Api\RecordController@store');
          // Изменить
          Route::post('/update', 'Api\RecordController@update');
          // Удалить
          Route::post('/delete', 'Api\RecordController@delete');
      });


      // Комменты
      Route::group(['prefix' => 'comments'], function () {
          // Получить
          Route::get('/index', 'Api\CommentController@index');
          // Создать
          Route::post('/store', 'Api\CommentController@store');
          // Изменить
          Route::post('/update', 'Api\CommentController@update');
          // Удалить
          Route::post('/delete', 'Api\CommentController@delete');
      });
});
